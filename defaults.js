function defaults(object, defaultProps) {
  for (let key in defaultProps) {
    if (object[key] == undefined) {
      object[key] = defaultProps[key];
    }
  }

  return object;
}

export default defaults;
