function keys(object) {
  let objectKeys = [];

  for (let key in object) {
    objectKeys.push(key);
  }

  return objectKeys;
}

export default keys;
