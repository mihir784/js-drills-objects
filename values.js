function values(object) {
  let objectValues = [];

  for (let key in object) {
    objectValues.push(object[key]);
  }

  return objectValues;
}

export default values;
