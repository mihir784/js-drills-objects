function mapObject(object, cb) {
  let mappedObject = {};

  for (let key in object) {
    mappedObject[key] = cb(object[key]);
  }

  return mappedObject;
}

export default mapObject;
