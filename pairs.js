function pairs(object) {
  let objectPairs = [];

  for (let key in object) {
    objectPairs.push([key, object[key]]);
  }

  return objectPairs;
}

export default pairs;
