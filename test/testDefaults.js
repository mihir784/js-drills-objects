import testObject from "../testObject.js";
import defaults from "../defaults.js";

const defaultProps = {
  name: "Bruce Wayne",
  age: 40,
  superPower: "I am rich!!!",
};

const result = defaults(testObject, defaultProps);
console.log(result);
