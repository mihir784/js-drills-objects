import testObject from "../testObject.js";
import mapObject from "../mapObject.js";

function mapped(value) {
  return "Mapped " + value;
}

const result = mapObject(testObject, mapped);
console.log(result);
